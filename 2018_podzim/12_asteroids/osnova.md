# Task to create with pyladies a working game of asteroids; part2

00) endless space
    (the spaceship can get out of window)
    spaceship x and y are compared with 0 and window width and
    then reset to 0
    00.py

01) not blinking endlessness
    (the spaceship blinks when overlapping)
    paint the screen all around the screen again
    10.py

02) asteroid!
    (we have none)
    copy the spaceship
    change image url
    remove reactions of asteroid
    put asteroid to the list of objects
    20.py

03) space object
    (we have too much duplicit code)
    create common class
    inherit
    remove duplicit code
    30.py

04) cleanup loading images
    (we load every image every time, fix asap)
    create load image function
    load images into predefined variables
    parametrize spaceobject init
    use image in asteroid and saceship
    40.py

05) asteroid start on the edge
    (this is just weird now)
    space object get x, y from params
    spaceship uses it with middle of the screen
    asteroid uses it with random edge of the screen
    50.py

06) cleanup speeds
    (we start to have everything everywhere)
    spaceship speed
    spaceship rotation speed
    asteroid speed
    asteroid rotation speed
    60.py

07) let asteroid rotate!
    (it doesn't)
    in tick, update rotation
    60.py

08) many asteroids!
    (we have just one now)
    add em when creating asteroids
    80.py

09) other sizes of asteroids
    (asteroids have only one size)
    parametrize constructor
    also add random asteroid textures
    try it out!
    90.py

## Collisions!
    objects have distance
        pythagoras would tell us
    objects shall have size
        distance - (a.size + b.size) > 0 is ok

10) show object size!
    (they don't colide)
    show object size (write the code?)
    a0.py

11) detect colision
    (it does not)
    define overlap function
    if spaceship overlaps asteroid,
        destroy spaceship sprite
        remove spaceship from objects
    b0.py

12) shoot
    (cannot destroy asteroid)
    create laser
    c0.py

13) shoot delay
    (too many lasers)
    create period
    d0.py

14) shoot lifetime
    (lasers forever)
    create lifetime
    e0.py

15) destroying objects
    (duplicit code)
    put destroy into common object
    f0.py

16) laser vs asteroid
    (destroy asteroid!)
    laser destroy asteroid if overlapping!
    g0.py

17) remove circles
    (we don't need em anymore)
    h0.py

18) split asteroids in many
    (so the game is almost done)
    h0.py

19) i found a bug when laser kills two asteroids at once! Fixed
    also some code-nice actions made
    i0.py













