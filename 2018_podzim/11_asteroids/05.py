import pyglet

WIDTH=800
HEIGHT=600

batch = pyglet.graphics.Batch()

class SpaceShip:
    def __init__(self):
        image_url = 'assets/PNG/playerShip2_red.png'
        image = pyglet.image.load(image_url)
        self.sprite = pyglet.sprite.Sprite(image, batch=batch)

        self.x = WIDTH /2
        self.y = HEIGHT /2

        self.x_speed = 0
        self.y_speed = 0

    def tick(self, dt):
        self.x += self.x_speed*dt
        self.y += self.y_speed*dt

        self.sprite.x = self.x
        self.sprite.y = self.y

objects = [SpaceShip()]

def draw():
    window.clear()
    batch.draw()

def tick(dt):
    for obj in objects:
        obj.tick(dt)

pyglet.clock.schedule(tick)
window = pyglet.window.Window(width=WIDTH, height=HEIGHT)
window.push_handlers(
        on_draw=draw,
)

pyglet.app.run()
