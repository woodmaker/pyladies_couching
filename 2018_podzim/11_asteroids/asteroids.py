import pyglet
import math

WIDTH=800
HEIGHT=600

batch = pyglet.graphics.Batch()

class SpaceShip:
    def __init__(self):
        self.x = WIDTH /2
        self.y = HEIGHT /2
        self.rotation = 90  # degrees
        self.rotation_speed = 200
        self.acceleration = 300

        image_url = 'assets/PNG/playerShip2_red.png'
        image = pyglet.image.load(image_url)
        image.anchor_x = image.width // 2
        image.anchor_y = image.height // 2
        self.sprite = pyglet.sprite.Sprite(image, batch=batch)

        self.x_speed = 0
        self.y_speed = 0

    def draw(self):
        self.sprite.x = self.x
        self.sprite.y = self.y
        self.sprite.rotation = 90 - self.rotation
        self.sprite.draw()

    def tick(self, dt):
        if pyglet.window.key.LEFT in pressed_keys:
            self.rotation += self.rotation_speed*dt
        if pyglet.window.key.RIGHT in pressed_keys:
            self.rotation -= self.rotation_speed*dt
        if pyglet.window.key.UP in pressed_keys:
            rot_rad = math.radians(self.rotation)
            self.x_speed += dt*self.acceleration*math.cos(rot_rad)
            self.y_speed += dt*self.acceleration*math.sin(rot_rad)

        self.x += self.x_speed*dt
        self.y += self.y_speed*dt

objects = [SpaceShip()]
pressed_keys = set()

def key_pressed(key, mod):
    pressed_keys.add(key)

def key_released(key, mod):
    pressed_keys.discard(key)

def draw():
    window.clear()
    for obj in objects:
        obj.draw()

def tick(dt):
    for obj in objects:
        obj.tick(dt)

pyglet.clock.schedule(tick)
window = pyglet.window.Window(width=WIDTH, height=HEIGHT)
window.push_handlers(
        on_draw=draw,
        on_key_press=key_pressed,
        on_key_release=key_released,
)

pyglet.app.run()
