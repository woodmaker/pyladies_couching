import pyglet

WIDTH=800
HEIGHT=600

batch = pyglet.graphics.Batch()

class SpaceShip:
    def __init__(self):
        image_url = 'assets/PNG/playerShip2_red.png'
        image = pyglet.image.load(image_url)
        self.sprite = pyglet.sprite.Sprite(image, batch=batch)

objects = [SpaceShip()]

def draw():
    batch.draw()

window = pyglet.window.Window(width=WIDTH, height=HEIGHT)
window.push_handlers(
        on_draw=draw,
)

pyglet.app.run()
