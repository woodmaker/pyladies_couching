# Task to create with pyladies a working game of asteroids

// I will count steps in 16base in 10s to ease adding steps

30) create window
    01.py

We should be sure here with the assets. TODO: make it.

40) create a spaceship
    (there is no spaceship)
    add a pyglet graphics batch
    add a SpaceShip class
        loading an image url
        creating a pyglet sprite
            into batch
            and into self.sprite
    create a list of space objects
        holding one SpaceShip()
    define a draw method
        calling draw on the batch
    push the handler of draw method to window
    * a lot of steps, reconsider, trim
    02.py

50) position spaceship
    (the spaceship is on left bottom)
    to the middle of the window
    * possible thing: put it on 0x.0y and ask? Too stupid?
    03.py

60) tick
    (nothing is moving)
    pyglet clock schedule calls periodically
        method tick
        default interval
        provides delta time
    method tick calls the method tick on all the space objects
    every object
        updates its properties
        reconfigures its sprite
    04.py

70) clear screen and provide speed to the ship
    (screen is not clearing well)
    (speed is constant which doesn't look good)
    clear window periodically
    set spaceship speed to 0
    using tick update using spaceship speed
    05.py

80) accelerate on key down
    (it does not react)
    create set of pressed keys
    def key_pressed function
    def key_released function
    tell window about it
    put conditional in spaceship tick
    06.py

90) rotate
    (it goes just one way)
    set a rotation to the object (in degrees)
    this rotation sets the sprite rotation
    define ROTATION_SPEED
    rotate on LEFT
    rotate on RIGHT
    07.py

A0) rotate in the middle
    (it rotates weird)
    set middle of image to the middle of image
    08.py

B0) go forward
    (it accelerates wrong way)
    how much which way?
    rocket, xy, how far in 1s, how does it affects xy,
    plot for x, plot for y, sin, cos, rad, ...

