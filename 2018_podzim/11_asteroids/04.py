import pyglet

WIDTH=800
HEIGHT=600

batch = pyglet.graphics.Batch()

class SpaceShip:
    def __init__(self):
        image_url = 'assets/PNG/playerShip2_red.png'
        image = pyglet.image.load(image_url)
        self.sprite = pyglet.sprite.Sprite(image, batch=batch)
        self.x = WIDTH /2
        self.y = HEIGHT /2

    def tick(self, dt):
        self.x += 20*dt
        self.y += 20*dt

        self.sprite.x = self.x
        self.sprite.y = self.y

objects = [SpaceShip()]

def draw():
    batch.draw()

def tick(dt):
    for obj in objects:
        obj.tick(dt)

pyglet.clock.schedule(tick)

window = pyglet.window.Window(width=WIDTH, height=HEIGHT)
window.push_handlers(
        on_draw=draw,
)

pyglet.app.run()
